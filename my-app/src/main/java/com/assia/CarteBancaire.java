package Nom;

import java.lang.String;

public class CarteBancaire {

    private int solde ;
    private int credit = 0;
    private int debit = 0 ;
    private int virement = 0 ;

    public CarteBancaire(int solde){
        this.solde = solde;
    }

    public String consulterSolde(){
        return "Solde = " + this.solde ;
    }

    public void debit(int debit)
    throws DecouvertException, ParametreNegatifException, SommeInferieurSoldeException
    {
        int verifier = this.solde ;

        if( verifier - debit < 0) throw new DecouvertException();

        if(debit < 0) throw new ParametreNegatifException();

        if(debit > this.solde) throw new ParametreNegatifException();

        else this.solde = this.solde - debit;

        consulterSolde();
    }

    public void credit(int credit)
    throws ParametreNegatifException
    {
        if(credit < 0) throw new ParametreNegatifException();

        else this.solde = this.solde + credit ;

        consulterSolde();
    }

    public void faireVirement(CarteBancaire destinataire, int virement)
        throws DecouvertException, ParametreNegatifException, SommeInferieurSoldeException
        {
            int verifier = this.solde ;
            if( verifier - virement < 0)
                throw new DecouvertException();

            if(virement < 0) throw new ParametreNegatifException();

            if(virement > this.solde) throw new SommeInferieurSoldeException();

            else
            {
                this.solde = this.solde - virement;
                destinataire.solde = destinataire.solde + virement;
            }
            consulterSolde();
        }
}



